import numpy as np
import torch
import torch.nn as nn

torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

class NetList(torch.nn.Module):
    def __init__(self, list_of_models):
        super(NetList, self).__init__()
        self.models = torch.nn.ModuleList(list_of_models)
    
    def forward(self, x, idx=0):
        return self.models[idx](x)

class LogReg(nn.Module):
    def __init__(self, input_dim):
        super(LogReg, self).__init__()
        self.fc = nn.Linear(input_dim, 1)
        
    def forward(self, x):
        x = self.fc(x)
        return x
        #x = torch.sigmoid(x)
        #x = torch.cat((x, 1 - x), dim=1)
        #return torch.log(x)

class rnn_classify(nn.Module):
    def __init__(self, in_feature=28, hidden_feature=10, num_class=1, num_layers=2):
        super(rnn_classify, self).__init__()
        self.rnn = nn.LSTM(in_feature, hidden_feature, num_layers)#使用两层lstm
        self.classifier = nn.Linear(hidden_feature, num_class)#将最后一个的rnn使用全连接的到最后的输出结果
    def forward(self, x):
        #x的大小为（batch，1，28,28），所以我们需要将其转化为rnn的输入格式（28，batch，28）
        x = x.reshape(x.shape[0],28,28)
#         print(x.size())
#         x = x.squeeze() #去掉（batch，1,28,28）中的1，变成（batch， 28,28）
        x = x.permute(2, 0, 1)#将最后一维放到第一维，变成（28,batch，28）
        out, _ = self.rnn(x) #使用默认的隐藏状态，得到的out是（28， batch， hidden_feature）
        out = out[-1,:,:]#取序列中的最后一个，大小是（batch， hidden_feature)
        out = self.classifier(out) #得到分类结果
        return out

     
class DNN(nn.Module):
    def __init__(self, input_dim, m=[8, 8]):
        super(DNN, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(input_dim, m[0]),
            nn.ReLU(),
            nn.Linear(m[0], m[1]),
            nn.ReLU(),
            nn.Linear(m[1], 1),
        )
        
    def forward(self, x):
        x = self.layers(x)
        return x
        #x = torch.sigmoid(x)
        #x = torch.cat((x, 1 - x), dim=1)
        #return torch.log(x)
