# python train.py --target adult --model dnn --seed 0 --gpu 1

# python infl.py --target adult --model dnn --type sgd --seed 0 --gpu 1

# python infl.py --target adult --model dnn --type true --seed 0 --gpu 1

# python infl.py --target adult --model dnn --type icml --seed 0 --gpu 1

# python train.py --target 20news --model dnn --seed 0 --gpu 1

# python infl.py --target 20news --model dnn --type sgd --seed 0 --gpu 1

# python infl.py --target 20news --model dnn --type true --seed 0 --gpu 1

# python infl.py --target 20news --model dnn --type icml --seed 0 --gpu 1


# python train.py --target mnist --model dnn --seed 0 --gpu 1

# python infl.py --target mnist --model dnn --type sgd --seed 0 --gpu 1

# python infl.py --target mnist --model dnn --type true --seed 0 --gpu 1

# python infl.py --target mnist --model dnn --type icml --seed 0 --gpu 1

python train.py --target mnist --model rnn --seed 0 --gpu 1

python infl.py --target mnist --model rnn --type sgd --seed 0 --gpu 1

python infl.py --target mnist --model rnn --type true --seed 0 --gpu 1

python infl.py --target mnist --model rnn --type icml --seed 0 --gpu 1

# https://discuss.pytorch.org/t/runtimeerror-derivative-for-cudnn-rnn-backward-is-not-implemented/50509
# https://github.com/facebookresearch/higher/issues/4
# https://blog.csdn.net/xckkcxxck/article/details/82978942